//
//  DetailView.swift
//  LipaDemo
//
//  Created by Josef Moser on 04.03.23.
//

import SwiftUI

struct DetailView: View {
    @Binding var isPresented: Bool
    let block: Block
    
    var body: some View {
        NavigationView {
            VStack(alignment: .leading, spacing: 10) {
                Text("Date: \(block.formattedDate())")
                Text("Block Height: \(block.id)")
                Text("Block Size: \(block.sizeString())")
                Text("Number of Transactions: \(block.transactionCount)")
                Spacer()
            }
            .navigationBarItems(trailing: Button(action: {
                isPresented.toggle()
            }, label: {
                Text("Close")
            }))
            .navigationBarTitle("Block #\(block.id)")
        }
    }
}
    
