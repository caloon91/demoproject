//
//  Model.swift
//  LipaDemo
//
//  Created by Josef Moser on 04.03.23.
//

import Foundation

struct Block: Identifiable {
    var timestamp: Int
    var id: Int // block height serves as id
    var size: Int
    var transactionCount: Int
    
    func sizeString() -> String {
        let sizeInMb = Double(self.size)/(1024.0*1024.0)
        return String(format: "%.2f", sizeInMb) + " MB"
    }
    func formattedDate() -> String {
        let date = Date(timeIntervalSince1970: TimeInterval(self.timestamp))
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .medium
        dateFormatter.timeStyle = .short
        
        return dateFormatter.string(from: date)
    }
}
