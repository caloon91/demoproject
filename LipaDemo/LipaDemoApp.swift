//
//  LipaDemoApp.swift
//  LipaDemo
//
//  Created by Josef Moser on 04.03.23.
//

import SwiftUI

@main
struct LipaDemoApp: App {
    var body: some Scene {
        WindowGroup {
            ListView()
        }
    }
}
