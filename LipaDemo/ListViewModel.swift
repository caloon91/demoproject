//
//  ListViewModel.swift
//  LipaDemo
//
//  Created by Josef Moser on 04.03.23.
//

import Foundation
import Alamofire

class ListViewModel: ObservableObject {
    @Published var blocks: Array<Block> = []
    
    init() {
        AF.request("https://mempool.space/api/v1/blocks/")
            .responseJSON { response in
                guard case .success(let blocks as Array<Any>) = response.result else {
                    // TODO: error
                    return
                }
                
                for i in 0...14 {
                    guard let block = blocks[i] as? Dictionary<String, Any> else {
                        // TODO: error
                        return
                    }
                    guard let timestamp = block["timestamp"] as? Int, let height = block["height"] as? Int, let size = block["size"] as? Int, let txcount = block["tx_count"] as? Int else {
                        // TODO: error
                        return
                    }
                    self.blocks.append(Block(timestamp: timestamp, id: height, size: size, transactionCount: txcount))
                }
                
            }
    }
}
