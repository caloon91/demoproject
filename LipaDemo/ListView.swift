//
//  ContentView.swift
//  LipaDemo
//
//  Created by Josef Moser on 04.03.23.
//

import SwiftUI

struct ListView: View {
    @ObservedObject var viewModel = ListViewModel()
    
    var body: some View {
        NavigationView {
            List(viewModel.blocks) { block in
                Row(block: block)
            }
            .navigationBarTitle("Recent Blocks")
            .padding()
        }
        
    }
}
    
struct Row: View {
    let block: Block
    @State private var showingDetail = false
    
    var body: some View {
        HStack {
            Text("\(block.id)")
                .font(Font.system(size: 20, weight: .bold))
            Spacer()
            Image(systemName: "chevron.right")
        }
        .onTapGesture {
            showingDetail.toggle()
        }
        .sheet(isPresented: $showingDetail) {
            DetailView(isPresented: $showingDetail, block: block)
        }
    }
}
struct ListView_Previews: PreviewProvider {
    static var previews: some View {
        ListView()
    }
}
